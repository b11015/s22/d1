// Array Methods

// Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits);

// push()
/*
	Syntax:
		arrayName.push("value");
*/

// PUSH is adding into the last elements

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength); //result: 5

console.log("Mutated through push: " + fruits);
console.log(fruits);

// add multiple elements in an array

fruits.push("Avocado", "Guava");
console.log("Mutated with multiple elements: " + fruits)
console.log(fruits);

// pop();
/*
	Syntax:
		arrayName.pop();
*/
// POP is removing last elements and show what was deleted element
let removeFruits = fruits.pop();
console.log(removeFruits) //result: Guava

console.log("Mutated through pop: " + fruits)
console.log(fruits);

// unshift();
/*
	Syntax:
		arrayName.unshift("value");
		arrayName.unshift("valueA", valueB);
*/

// insert in forward from 0 and then will replace Lime and Banana
fruits.unshift("Lime", "Banana");
console.log("Mutated through unshift: " + fruits)
console.log(fruits);

// shift()
/*
	Syntax:
		arrayName.shift();
*/

// papakita nito kung sinung element ang matatanggal is 1st element
let anotherFruit = fruits.shift(); //result: Lime
console.log(anotherFruit);
console.log(fruits);

// function removeFirst(){
// 	fruits.shift()
// 	console.log(fruits);
// 	// return fruits;
// }
// // global variable if you use return
// removeFirst();

// splice()
/*
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated through splice: " + fruits)
console.log(fruits); //result: remove apple and orange

fruits.splice(3, 1, "Orange");
console.log("Mutated through splice again " + fruits);
console.log(fruits);

fruits.splice(0, 2);
console.log("Mutated through splice without adding: " + fruits)
console.log(fruits)

// sort();
/*
	Syntax
		arrayName.sort();
*/

fruits.sort();
console.log("Mutated through sort: " + fruits)
console.log(fruits);

// reverse()
/*
	Syntax:
		arrayName.reverse();
*/

fruits.reverse()
console.log("Mutated through reverse " + fruits)
console.log(fruits);


// Non-Mutator Methods
/*
	- Non-Mutator methods are function that do not modify or change an array after they're created
	- These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
*/

console.log("Non-Mutator Methods starts here:")
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
let countries2 = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE", "PH"];

// indexOf()
/*
	Syntax:
		arrayName.indexOf(searchValue)
*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf " + firstIndex); //result: 1

let invalidCountry = countries.indexOf("BR")
console.log("Result of indexOf again: " + invalidCountry); //result: -1 beacause wala yung country

// lastIndexOf()
/*
	Syntax:
		arrayName.lastIndex(searchValue)
		arrayName.lastIndexOf(searchValue, fromIndex)
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf " + lastIndex); //result: 5

let lastIndexStart = countries2.lastIndexOf("PH", 7);
console.log(countries2[7]);
console.log("Result of lastIndexOf again " + lastIndexStart)
// reslt 5
// console.log(countries);

// slice()
/*
	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

let slicedArrayA = countries.slice(2);
console.log("Result from slice with startingIndex only: " + slicedArrayA);
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice with startingIndex and endingIndex: " + slicedArrayB);
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice with negative value: " + slicedArrayC);
console.log(slicedArrayC);
console.log(countries);

// toString()
/*
	Syntax:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result from toString: ")
console.log(stringArray);

// concact();
/*
	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe bootstrap'];
let tasksArrayC = ['get git', 'be node']

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method: ')
console.log(tasks);

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat with all arrays: ')
console.log(allTasks)

// combining arrays with elements

let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat with elements: ')
console.log(combinedTasks);

// join()
/*
	Syntax:
		arrayName.join('separator string')
*/

let users = ["John", "Robert", "Jane", "Joe"];
console.log(users.join()); //result: John,Robert,Jane,Joe
// separator: comma (,)
console.log(users.join(' ')); //result: John Robert Jane Joe
// separator: no spaces
console.log(users.join(' - '));
//  result: John - Robert - Jane - Joe

//  Iteration Methods

// forEach()
/*
	Syntax:
		arrayName.forEach(function(individualElement){statement / code block});
*/

allTasks.forEach(function(task){
	console.log(task);
})

// use  forEach with conditional statements

let filteredTasks = [];

allTasks.forEach(function(task){
	console.log(task);

	if(task.length > 10){
		console.log(task);
		filteredTasks.push(task)
	}
})

console.log('Result of filteredTasks: ')
console.log(filteredTasks);

// map();
/*
	Syntax:
		let / const resultArray = arrayName.map(function(individualElement){
			return statement/ code block
		})
*/

let numbers  = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number;
});

console.log('Original array:')
console.log(numbers); //result: original is unaffected by map()
console.log('Result of map method')
console.log(numberMap); //result: a new array returned by map() and stored in a variable

// forEach() vs. map()
let numberForEach = numbers.forEach(function(number){
	return number * number;
});
console.log(numberForEach); //result: undefined

// every()
/*
	Syntax:
		let / const resultArray = arrayName:every(function(individualElement){
			return code block / statement (condition)
		})
*/

let allValid = numbers.every(function(number){
	return (number < 3)
});

console.log('Result from every method:')
console.log(allValid); //result: false

// some()
/*
	Syntax:
		let / const resultArray = arrayName.some(function(){
			return statement / code block (condition)
		})
*/

let someValid = numbers.some(function(number){
	return (number < 2)
});

console.log('Result from some method:');
console.log(someValid);

if(someValid){
	console.log('Some numbers in the array are less than 2.')
};

// filter()
/*
	Syntax:
		let / const resultArray = numbers.filter(function(individualElement){
			return statement code block (condition)
		})
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
});

console.log('Result of filter method')
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number > 5)
})
console.log('Result of filter method with nothing found: ');
console.log(nothingFound);

// function filtering(){
// 	let string = 'supercalifragilisticexpialidpcious';
// 	let stringResult = string.filter(function(letter){
// 		(
// 			name[i].toLowerCase() == "a" ||
// 			name[i].toLowerCase() == "e" ||
// 			name[i].toLowerCase() == "i" ||
// 			name[i].toLowerCase() == "o" ||
// 			name[i].toLowerCase() == "u" 		}
// 	})
// }

// let stringResult = filtering();
// console.log(stringResult)

// filtering using forEach()

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
});
console.log('Filtering through forEach:');
console.log(filteredNumbers);

// reduce()
/*
	Syntax:
		let / const resultArray = arrayName.reduce(function(accumulator, currentValue){
			return expression / operation
		})
*/

let iteration = 0;
let reduceArray = numbers.reduce(function(x, y){
	console.warn('current iteration: ' + ++iteration)
	console.log('accumulator: ' + x)
	console.log('currentValue: ' + y)

	// operation
	return x + y
});
console.log('Result from reducedArray: ' + reduceArray);